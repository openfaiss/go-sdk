module gitee.com/openfaiss/go-sdk

go 1.19

require (
	gitee.com/openfaiss/faiss-provider v0.0.0-20231025154314-b3cd5a9b4c9e
	github.com/google/go-cmp v0.5.9
)
